# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Drawing tool 選單有六種工具：Pencil, Eraser, Rectangle, Circle, Triangle和Text。三種形狀都是空心。
    
    Color選單利用input type="color"的選單來選擇顏色，對於Eraser以外的工具都有效。
    
    Width選單有六種粗細，對於所有的工具都有效。若使用Text工具，字體大小為Width*5。
    
    Font選單有三種字體，只對Text工具有效。
    
    Text欄位在輸入文字之後，如果當時選擇Text工具，在畫面上點一下就會出現輸入的文字。
    
    Reset, Undo和Redo的功能如同按鈕上的文字所描述。
    
    按下「選擇檔案」之後，可以上傳圖片取代目前的圖片。
    
    按下Download連結之後，可以下載目前的圖片。

### Function description

    img_update:六種工具實際上是在canvas元件上操作，放開滑鼠之後，都會呼叫img_update函數，把這次的操作更新到canvaso元件（目前畫面所顯示的內容）上。使用Reset或是Upload工具也會呼叫img_update。在img_update函數中，會同時在「canvas陣列」array最後面加入目前canvaso的內容，用來支援Undo和Redo。

    tools.pencil:按下滑鼠的時候，首先偵測目前的顏色和寬度，並修改滑鼠圖示。用一個變數started紀錄是否已經按下滑鼠。按下滑鼠之後的每一個時間點，從上一個時間點的位置畫一條線到現在的位置。放開滑鼠之後，將started改成false，就不會繼續畫線。
    
    tools.eraser:與Pencil幾乎相同，唯一的差別是使用globalCompositeOperation='destination-out'來將經過的路徑的內容改成透明。
    
    tools.rectangle:按下滑鼠的時候，首先偵測目前的顏色和寬度，並修改滑鼠圖示。用一個變數started紀錄是否已經按下滑鼠。按下滑鼠之後的每一個時間點，用strokeRect畫一個長方形，以按下滑鼠的位置和目前滑鼠的位置為兩個頂點畫一個長方形。放開滑鼠之後，將started改成false，長方形就不會再繼續隨著滑鼠更新。
    
    tools.circle:與Rectangle幾乎相同，唯一的差別是在滑鼠按住的每個時間點，用arc畫出一個圓，圓心是按下滑鼠的位置，半徑是目前滑鼠的位置和按下滑鼠的位置的距離。
    
    tools.triangle:與Rectangle幾乎相同，唯一的差別是在滑鼠按住的每個時間點，用三次lineTo畫出一個正三角形，中心是按下滑鼠的位置，中心到三個頂點的距離都是目前滑鼠的位置和按下滑鼠的位置的距離。
    
    tools.text:按下滑鼠的時候，偵測目前的顏色、寬度、字體和Text欄位的資料，用fillText在滑鼠按下的位置出現Text欄位的文字。
    
    ev_reset:直接使用clearRect來把整個畫面改成透明。
    
    ev_undo:用一個變數index紀錄目前的畫面是array陣列中的第幾個。img_update之後，index一定是array.length()-1。按下Undo按鈕後，如果index>0，就把index減一，並更新畫面內容。
    
    ev_redo:與Undo幾乎相同，唯一的差別是按下Redo按鈕後，如果index<array.length()-1，就把index加一，並更新畫面內容。
    
    ev_download:把目前畫面的內容存到一張隱形的圖片中，並在按下超連結的時候用HTML裡面超連結的設定下載那張圖片。（按鈕似乎無法下載，所以使用超連結。）
    
    ev_upload:取得上傳的圖片之後，先把目前的畫面清空，再把這張圖片畫上去。array和index也要更新，才能正常支援Undo和Redo。
    
    程式碼部分參考自：
    
    https://dev.opera.com/articles/html5-canvas-painting/
    
    https://developer.mozilla.org/zh-TW/docs/Web/API/Canvas_API/Tutorial
    
    http://www.htmleaf.com/ziliaoku/qianduanjiaocheng/201502151385.html
    
    https://wcc723.github.io/canvas/2014/12/09/html5-canvas-03/
    
    https://blog.xuite.net/vexed/tech/56169024-HTML5+Canvas+另存圖檔
    
    https://blog.darkthread.net/blog/html5-canvas-sktechpad/

### Gitlab page link

    https://107000128.gitlab.io/AS_01_WebCanvas

### Others (Optional)

