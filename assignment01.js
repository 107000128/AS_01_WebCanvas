window.addEventListener('load', function () {
    var canvas, context, canvaso, contexto, array = new Array();
    var index = -1;
    var tool = 'pencil';
    var tool_default = 'pencil';
    
    function init () {
        // Find the canvas element.
        canvaso = document.getElementById('imageView');
        
        // Get the 2D canvas context.
        contexto = canvaso.getContext('2d');
        
        // Add the temporary canvas.
        var container = canvaso.parentNode;
        canvas = document.createElement('canvas');
        
        canvas.id     = 'imageTemp';
        canvas.width  = canvaso.width;
        canvas.height = canvaso.height;
        container.appendChild(canvas);
        
        context = canvas.getContext('2d');
        
        // Get the changes in the menu.
        var tool_select = document.getElementById('dtool');
        tool_select.addEventListener('change', ev_tool_change, false);

        var color_select = document.getElementById('dcolor');
        color_select.addEventListener('change', ev_color_change, false);

        var width_select = document.getElementById('dwidth');
        width_select.addEventListener('change', ev_width_change, false);

        var font_select = document.getElementById('dfont');
        font_select.addEventListener('change', ev_font_change, false);

        var reset = document.getElementById('reset');
        reset.addEventListener('click', ev_reset, false);
        
        var undo = document.getElementById('undo');
        undo.addEventListener('click', ev_undo, false);

        var redo = document.getElementById('redo');
        redo.addEventListener('click', ev_redo, false);

        var download = document.getElementById('save');
        download.addEventListener('click', ev_download, false);

        var upload = document.getElementById('upload');
        upload.addEventListener('change', ev_upload, false);
        
        //Activate default tool.
        if (tools[tool_default]) {
            tool = new tools[tool_default]();
            tool_select.value = tool_default;
        }
        
        // Attach the mousedown, mousemove and mouseup event listeners.
        canvas.addEventListener('mousedown', ev_canvas, false);
        canvas.addEventListener('mousemove', ev_canvas, false);
        canvas.addEventListener('mouseup',   ev_canvas, false);
        
        img_update();
    }
    
    // This function determines the mouse position relative to the canvas.
    function ev_canvas (ev) {
        if (ev.layerX || ev.layerX == 0) { // Firefox
            ev._x = ev.layerX;
            ev._y = ev.layerY;
        } else if (ev.offsetX || ev.offsetX == 0) { // Opera
            ev._x = ev.offsetX;
            ev._y = ev.offsetY;
        }
        
        // Call the event handler of the tool.
        var func = tool[ev.type];
        if (func) {
            func(ev);
        }
    }

    var t = 'pencil';
    var c = 'black';
    var w = 1;
    var f = 'arial';
    
    // The event handlers for changes made to any of the selector.
    function ev_tool_change (ev) {
        if (tools[this.value]) {
            tool = new tools[this.value]();
        }
    }
    
    function ev_color_change (ev) {
        c = this.value;
    }
    
    function ev_width_change (ev) {
        if (widths[this.value]) {
            width = new widths[this.value]();
            w = width.width;
        }
    }
    
    function ev_font_change (ev) {
        if (fonts[this.value]) {
            font = new fonts[this.value]();
            f = font.font;
        }
    }
    
    function ev_reset (ev) {
        console.log("reset");
        contexto.clearRect(0, 0, canvas.width, canvas.height);
        img_update();
    }
    
    function ev_undo (ev) {
        if (index > 0) {
            index--;
            console.log(index);
            console.log(array.length);
            var image = new Image();
            image.src = array[index];
            image.onload = function () {
                contexto.clearRect(0, 0, canvas.width, canvas.height);
                contexto.drawImage(image, 0, 0);
            }
        } else {
            console.log("ERROR!");
        }
    }
    
    function ev_redo (ev) {
        if (index < array.length - 1) {
            index++;
            console.log(index);
            console.log(array.length);
            var image = new Image();
            image.src = array[index];
            image.onload = function () {
                contexto.clearRect(0, 0, canvas.width, canvas.height);
                contexto.drawImage(image, 0, 0);
            }
        } else {
            console.log("ERROR!");
        }
    }
    
    function ev_download (ev) {
        console.log("download");
        var image = document.getElementById('image');
        image.src = canvaso.toDataURL('image/png');
        this.href = image.src;
    }
    
    function ev_upload (ev) {
        console.log("upload");
        var image = document.getElementById('image');
        image.src = URL.createObjectURL(this.files[0]);
        image = document.getElementById('image');
        contexto.clearRect(0, 0, canvas.width, canvas.height);
        image.onload = function () {
            contexto.drawImage(image, 0, 0);
            index++;
            if (index < array.length) array.length = index;
            array.push(document.getElementById('imageView').toDataURL());
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
    }
    
    // This function is called each time when the user completes an operation.
    function img_update () {
        contexto.drawImage(canvas, 0, 0);
        index++;
        if (index < array.length) array.length = index;
        array.push(document.getElementById('imageView').toDataURL());
        console.log(index);
        console.log(array.length);
        context.clearRect(0, 0, canvas.width, canvas.height);
    }
    
    var tools = {};
    var colors = {};
    var widths = {};
    var fonts = {};
    
    // The pencil.
    tools.pencil = function () {
        var tool = this;
        this.started = false;
        canvas.style.cursor = 'crosshair';
        this.mousedown = function (ev) {
            context.beginPath();
            context.moveTo(ev._x, ev._y);
            tool.started = true;
            context.lineWidth = w;
            context.strokeStyle = c;
        };
        this.mousemove = function (ev) {
            if (tool.started) {
                context.lineTo(ev._x, ev._y);
                context.stroke();
            }
        };
        this.mouseup = function (ev) {
            if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
            }
        };
    };

    // The eraser.
    tools.eraser = function () {
        var tool = this;
        this.started = false;
        canvas.style.cursor = 'default';
        this.mousedown = function (ev) {
            context.beginPath();
            context.moveTo(ev._x, ev._y);
            tool.started = true;
            context.lineWidth = w;
            contexto.globalCompositeOperation = 'destination-out';
            context.strokeStyle = "rgba(255, 255, 255, 1)";
        };
        this.mousemove = function (ev) {
            if (tool.started) {
                context.lineTo(ev._x, ev._y);
                context.stroke();
            }
        };
        this.mouseup = function (ev) {
            if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
                contexto.globalCompositeOperation = "source-over";
            }
        };
    };

    // The rectangle tool.
    tools.rectangle = function () {
        var tool = this;
        this.started = false;
        canvas.style.cursor = 'move';
        this.mousedown = function (ev) {
            tool.started = true;
            tool.x0 = ev._x;
            tool.y0 = ev._y;
            context.lineWidth = w;
            context.strokeStyle = c;
        };
        this.mousemove = function (ev) {
            if (!tool.started) {
                return;
            }
            var x = Math.min(ev._x,  tool.x0),
                y = Math.min(ev._y,  tool.y0),
                w = Math.abs(ev._x - tool.x0),
                h = Math.abs(ev._y - tool.y0);
            context.clearRect(0, 0, canvas.width, canvas.height);
            if (!w || !h) {
                return;
            }
            context.strokeRect(x, y, w, h);
        };
        this.mouseup = function (ev) {
            if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
            }
        };
    };

    // The circle tool.
    tools.circle = function () {
        var tool = this;
        this.started = false;
        canvas.style.cursor = 'copy';
        this.mousedown = function (ev) {
            tool.started = true;
            tool.x0 = ev._x;
            tool.y0 = ev._y;
            context.lineWidth = w;
            context.strokeStyle = c;
        };
        this.mousemove = function (ev) {
            if (!tool.started) {
                return;
            }
            var r = Math.hypot(ev._x - tool.x0, ev._y - tool.y0);
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.beginPath();
            context.arc(tool.x0, tool.y0, r, 0, Math.PI * 2, true);
            context.stroke();
            context.closePath();
        };
        this.mouseup = function (ev) {
            if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
            }
        };
    };

    // The triangle tool.
    tools.triangle = function () {
        var tool = this;
        this.started = false;
        canvas.style.cursor = 'pointer';
        this.mousedown = function (ev) {
            tool.started = true;
            tool.x0 = ev._x;
            tool.y0 = ev._y;
            context.lineWidth = w;
            context.strokeStyle = c;
        };
        this.mousemove = function (ev) {
            if (!tool.started) {
                return;
            }
            var r = Math.hypot(ev._x - tool.x0, ev._y - tool.y0);
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.beginPath();
            context.moveTo(tool.x0, tool.y0 - r);
            context.lineTo(tool.x0 - Math.sqrt(3) / 2 * r, tool.y0 + r / 2);
            context.lineTo(tool.x0 + Math.sqrt(3) / 2 * r, tool.y0 + r / 2);
            context.lineTo(tool.x0, tool.y0 - r);
            context.moveTo(tool.x0 - Math.sqrt(3) / 2 * r, tool.y0 + r / 2);
            context.lineTo(tool.x0, tool.y0 - r);
            context.lineTo(tool.x0 + Math.sqrt(3) / 2 * r, tool.y0 + r / 2);
            context.stroke();
            context.closePath();
        };
        this.mouseup = function (ev) {
            if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
            }
        };
    };

    // The text tool.
    tools.text = function () {
        var tool = this;
        canvas.style.cursor = 'text';
        this.mouseup = function (ev) {
            context.font = w * 5 + "px " + f;
            context.fillStyle = c;
            context.fillText(document.getElementById("in").value, ev._x, ev._y);
            img_update();
        };
    };

    //Set width or fonts.
    widths.one = function () {this.width = 1;}
    widths.three = function () {this.width = 3;}
    widths.five = function () {this.width = 5;}
    widths.ten = function () {this.width = 10;}
    widths.fifteen = function () {this.width = 15;}
    widths.twenty = function () {this.width = 20;}
    fonts.arial = function () {this.font = 'Arial';}
    fonts.comic = function () {this.font = 'Comic Sans MS';}
    fonts.courier = function () {this.font = 'Courier New';}
    
    init();
}, false);
